package lesson2.exercise1;

/**
 * Created by stmirage on 06.10.16.
 */
public enum Label {
    SPAM, NEGATIVE_TEXT, TOO_LONG, OK
}

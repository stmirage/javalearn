package lesson2.exercise1;

/**
 * Created by stmirage on 08.10.16.
 */
public class StaticAnalyzerClass {
    public static Label StaticAnalyzer(TextAnalyzer[] textAnalyzers, String text){
        for(TextAnalyzer t : textAnalyzers){
            Label processedText = t.processText(text);
            if ( !processedText.equals(Label.OK) ) return processedText;
        }
        return Label.OK;
    }
}

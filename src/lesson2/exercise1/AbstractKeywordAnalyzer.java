package lesson2.exercise1;
import lesson2.exercise1.Label;
/**
 * Created by stmirage on 06.10.16.
 */
abstract class AbstractKeywordAnalyzer implements TextAnalyzer{
    protected Label label;

    abstract public String[] getKeywords();
    // Вообще, не вижу необходимости реализовывать getLabel в потомках. Вполне себе можно реализовать его неабстрактно
    // Но задание есть задание
    AbstractKeywordAnalyzer(Label label){
        this.label = label;
    }
    abstract public Label getLabel();
    public Label processText(String text){
        for (String s : getKeywords()){
            if (text.contains(s)){
                return label;
            }
        }
        return Label.OK;
    };
}

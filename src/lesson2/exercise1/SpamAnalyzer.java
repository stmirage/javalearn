package lesson2.exercise1;

/**
 * Created by stmirage on 08.10.16.
 */
public class SpamAnalyzer extends AbstractKeywordAnalyzer {
    private String[] keyWords;
    public SpamAnalyzer(String[] keyWords){
        super(Label.SPAM);
        this.keyWords = keyWords;
    }

    @Override
    public String[] getKeywords() {
            return keyWords;
    }

    @Override
    public Label getLabel() {
        return super.label;
    }
}

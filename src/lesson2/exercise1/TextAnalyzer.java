package lesson2.exercise1;

/**
 * Created by stmirage on 06.10.16.
 */
interface TextAnalyzer {
    Label processText(String text);
}
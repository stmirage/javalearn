package lesson2.exercise1;

import static lesson2.exercise1.StaticAnalyzerClass.StaticAnalyzer;

/**
 * Created by stmirage on 08.10.16.
 */
public class Runner {
    public static void main(String[] args){
        NegativeTextAnalyzer ngTextAnalyzer = new NegativeTextAnalyzer();
        //System.out.println(ngTextAnalyzer.processText(":)))):("));
        //System.out.println(ngTextAnalyzer.processText(":))))"));

        SpamAnalyzer spmTextAnalyzer = new SpamAnalyzer(new String[]{"1","2"});
        //System.out.println(spmTextAnalyzer.processText(":)))):("));
        //System.out.println(spmTextAnalyzer.processText("1"));

        TooLongTextAnalyzer tooLong = new TooLongTextAnalyzer(5);
        //System.out.println(tooLong.processText("1"));
        //System.out.println(tooLong.processText("111111"));

        System.out.println(StaticAnalyzer(new TextAnalyzer[]{ngTextAnalyzer,spmTextAnalyzer,tooLong}, ":)))):(11111"));
    }

}

package lesson2.exercise1;

/**
 * Created by stmirage on 08.10.16.
 */
public class NegativeTextAnalyzer extends AbstractKeywordAnalyzer {
    private String[] keyWords = {":(", "=(", ":|"};

    public NegativeTextAnalyzer() {
        super(Label.NEGATIVE_TEXT);
    }

    @Override
    public String[] getKeywords() {
        return keyWords;
    }

    @Override
    public Label getLabel() {
        return super.label;
    }
}

package lesson2.exercise1;

/**
 * Created by stmirage on 08.10.16.
 */
public class TooLongTextAnalyzer implements TextAnalyzer{
    /* на самом деле хранение во внутренней переменной значения Label.TOO_LONG излишне
    Метод processText должен выглядеть так
    @Override
    public Label processText(String text) {
        if (text.length() > maxLength){
            return Label.TOO_LONG;
        }
        return Label.OK;
    }
    */
    private Label label = Label.TOO_LONG;
    private int maxLength;
    public TooLongTextAnalyzer (int maxLength){
        this.maxLength = maxLength;
    }
    public Label getLabel() {
        return label;
    }
    @Override
    public Label processText(String text) {
        if (text.length() > maxLength){
            return label;
        }
        return Label.OK;
    }
}

package lesson2.exersise2;
import java.util.Stack;
import java.util.StringJoiner;

/**
 * Created by stmirage on 09.10.16.
 */
public class UnixWayParser {
    static public String Parser(String inWay){
        String result = "";
        Stack<String> stack = new Stack<>();
        String[] args = inWay.split("/");
        for(String s: args){
            switch (s){
                case("."):
                    break;
                case(".."):
                    stack.pop();
                    break;
                default:
                    stack.push(s);
            }

        };
        // Ну здесь только обход по стеку. сделал так, хотя вероятно в утилях есть какой-нить правильный toArray of String
        // С дальнейшей обработкой
        StringJoiner sj = new StringJoiner("/", "","");
        while (!stack.empty()){
            sj.add(stack.pop());
        }
        result = new StringBuilder(sj.toString()).reverse().toString();
        return result;
    }


    static public void main(String[] args){
        System.out.println(Parser("/a/b/c/../d"));
        System.out.println(Parser("/a/"));

    }
}

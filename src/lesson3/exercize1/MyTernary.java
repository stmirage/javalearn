package lesson3.exercize1;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
/**
 * Created by stmirage on 22.10.16.
 */
public class MyTernary {
    public static <T, U> Function<T, U> ternaryOperator(

            Predicate<? super T> condition, //Условие

            Function<? super T, ? extends U> ifTrue, //Верните значение этой функции, если условие истинно

            Function<? super T, ? extends U> ifFalse //Верните значение этой функции, если условие ложно

    ){

        return (T arg) -> condition.test(arg) ? ifTrue.apply(arg) : ifFalse.apply(arg);
    }


    public static void main(String[] args){
    Predicate<Object> condition = Objects::isNull;

    Function<Object, Integer> ifTrue = obj -> 0;

    Function<CharSequence, Integer> ifFalse = CharSequence::length;

    Function<String, Integer> safeStringLength = ternaryOperator(condition, ifTrue, ifFalse);
    System.out.println(safeStringLength.apply("Joe"));
        System.out.println(safeStringLength.apply(null));

}

}

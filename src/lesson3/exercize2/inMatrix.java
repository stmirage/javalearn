package lesson3.exercize2;

import java.io.*;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Created by stmirage on 22.10.16.
 */
public class inMatrix {

    LinkedList<Integer> result = new LinkedList<Integer>();
    LinkedList<LinkedList<Integer>> Matrix;
    public inMatrix(String fileName) throws IOException {


        File inFile = new File(fileName);
        BufferedReader inData;
        inData = new BufferedReader(new FileReader(inFile.getAbsoluteFile()));
        this.Matrix = new LinkedList<LinkedList<Integer>>();
        int rows=0,columns=0,requests=0;

        String[] params = inData.readLine().split(" ");

        rows = Integer.parseInt(params[0]);
        columns = Integer.parseInt(params[1]);
        requests = Integer.parseInt(params[2]);

        for(int i = 0;i<rows;i++){
                this.Matrix.add(i, new LinkedList<Integer>());
                String[] getRow = inData.readLine().split(" ");
                for(int j = 0;j<columns;j++){
                    this.Matrix.get(i).push(Integer.parseInt(getRow[columns-j-1]));

                }
        }


        for (int i=0;i<requests;i++){
            String[] request = inData.readLine().split(" ");
            switch (request[0].charAt(0)){
                case 'g':
                    readR(Integer.parseInt(request[1]),Integer.parseInt(request[2]));
                    break;
                case 'r':
                    changeRowR(Integer.parseInt(request[1]),Integer.parseInt(request[2]));
                    break;
                case 'c':
                    changeColumnC(Integer.parseInt(request[1]),Integer.parseInt(request[2]));
                    break;
                default:
                    break;

            }

        }
    Collections.reverse(result);
    }


    private void readR(Integer row,Integer column){
        this.result.push(Matrix.get(row-1).get(column-1));
    }

    private void changeRowR(Integer row1, Integer row2){
        LinkedList<Integer> tmp = this.Matrix.get(row1-1);
        this.Matrix.set(row1-1, this.Matrix.get(row2-1));
        this.Matrix.set(row2-1, tmp);
    }


    private void changeColumnC(Integer column1, Integer column2){
       for (LinkedList<Integer> row : this.Matrix){
           Integer tmp = row.get(column1-1);
           row.set(column1-1, row.get(column2-1));
           row.set(column2-1, tmp);
       }
    }

    public void printMatrix(){

            for(int i = 0;i<this.Matrix.size();i++){
                for(int j = 0;j<this.Matrix.get(i).size();j++){
                    System.out.print(this.Matrix.get(i).get(j) + " ");
                }
                 System.out.print("\n");
            }
            System.out.println();

    }

    public LinkedList<Integer> Out(){
        return this.result;
    }

    public void printOutPut(){
        result.forEach(System.out::println);
    }
}

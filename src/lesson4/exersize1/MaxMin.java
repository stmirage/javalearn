package lesson4.exersize1;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by stmirage on 02.11.16.
 */

/*

Ебушки-воробушки
 */
public class MaxMin {
    public static <T> void findLimitValues(
            Stream<? extends T> stream,
            Comparator<? super T> cmp)
    {
        List<T> source = stream.collect(Collectors.toList());
        if(source.isEmpty()){
            System.out.println("null");
            return;
        }
        T min = (T) source.stream().min(cmp).get();
        T max = (T) source.stream().max(cmp).get();


        System.out.println(min + " " + max);
    }







    public static void main(String[] args){
        final Integer[] iS = new Integer[]{1, 2, 3, 4};
        final Integer[] iS2 = new Integer[]{};
        findLimitValues(Arrays.stream(iS),new Comp());
        findLimitValues(Arrays.stream(iS2),new Comp());

    }

    private static class Comp implements Comparator<Integer>{
        @Override
        public int compare(Integer o1, Integer o2) {
            if (o1>o2) return 1;
            if (o1<o2) return -1;
            return 0;
        }

        @Override
        public Comparator<Integer> reversed() {
            return null;
        }

        @Override
        public Comparator<Integer> thenComparing(Comparator<? super Integer> other) {
            return null;
        }

        @Override
        public <U> Comparator<Integer> thenComparing(Function<? super Integer, ? extends U> keyExtractor, Comparator<? super U> keyComparator) {
            return null;
        }

        @Override
        public <U extends Comparable<? super U>> Comparator<Integer> thenComparing(Function<? super Integer, ? extends U> keyExtractor) {
            return null;
        }

        @Override
        public Comparator<Integer> thenComparingInt(ToIntFunction<? super Integer> keyExtractor) {
            return null;
        }

        @Override
        public Comparator<Integer> thenComparingLong(ToLongFunction<? super Integer> keyExtractor) {
            return null;
        }

        @Override
        public Comparator<Integer> thenComparingDouble(ToDoubleFunction<? super Integer> keyExtractor) {
            return null;
        }
    }
}

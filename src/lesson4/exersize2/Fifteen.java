package lesson4.exersize2;

import java.util.Comparator;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by stmirage on 02.11.16.
 */
public class Fifteen {
    public static void doExercize(String input){
        Comparator<Map.Entry<String, Long>> maxComparator = Comparator.comparing(Map.Entry::getValue, Comparator.reverseOrder());
        Comparator<Map.Entry<String, Long>> LexicOrderComaparator = Comparator.comparing(Map.Entry::getKey);


        Stream.of(input.split("\\s+"))
                .map(x -> x.toLowerCase()
                .replaceAll("[,.!:-]",""))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .sorted(maxComparator.thenComparing(LexicOrderComaparator))
                .limit(15)
                .forEach(System.out::println);

    }

    public static void main(String[] args){

        doExercize("21 12 Возникают некоторые вопросы, связанные с использованием. Философу цицерону, ведь именно из его трактата. Длине наиболее распространенных слов отступов. И по сей день более длинный неповторяющийся набор слов.. Могут возникнуть небольшие проблемы: в книгопечатании еще в качестве. Который планируется использовать в книгопечатании еще в качестве рыбы текст этот несмотря. Даже с использованием lorem ipsum на кириллический контент – написание символов." +
               "Добра и по сей день языками, использующими латинский алфавит, могут возникнуть. Совсем необязательно является знаменитый lorem ipsum. Слова, получив текст-рыбу, широко используемый и проектах, ориентированных на том языке. Возникнуть небольшие проблемы: в xvi веке абзацев, отступов. Небольшие проблемы: в качестве рыбы текст. Для вставки на латыни и слова, получив текст-рыбу, широко используемый и. Буквы встречаются с разной частотой, имеется разница в длине наиболее." +
               "Появлением lorem нечитабельность текста исключительно демонстрационная, то и проектах, ориентированных на кириллице. Не имеет никакого отношения к обитателям водоемов кириллический. Считается, что такое текст-рыба сей день связанные с разной. Демонстрационная, то и даже с языками, использующими латинский алфавит, могут возникнуть. Частотой, имеется разница в качестве. Оригинального трактата, благодаря чему появляется возможность получить более длинный неповторяющийся набор слов.. Даже с разной частотой, имеется разница в. ");
    }
}
